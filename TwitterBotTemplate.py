"""
This is a Twitter Bot Template.

Usage:
1. Rename the Class as desired.
2. Inherit from desired Base Class like BasicTwitterBot
"""
import BasicTwitterBot
import tweepy

config_layout_expansion = ["expansion1", "expansion2"]
_config_file = "config.txt"


class TemplateBot(BasicTwitterBot.BasicTwitterBot):
    def __init__(self, config_file: str):
        super().__init__(
            config_file, BasicTwitterBot.basic_config_keys + config_layout_expansion
        )

    def load_config(self):
        super().load_config()
        # Do some extra config stuff in here.
        return self

    def start(self):
        super().start()
        """Starts the RetweetBot instance and handles all the retweeting process."""
        # Do your bot stuff in here.


def main():
    try:
        TemplateBot(
            _config_file
        ).varify_config_file().load_config().authenticate().start()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    # execute only if run as a script
    main()
