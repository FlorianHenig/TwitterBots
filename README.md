# TwitterBots

A collection of Twitter Bots

# Kurzanleitung
## RetweetBot
- Retweeted alle Posts von Accounts einer oder mehrerer Twitter-Listen
- Retweetet nur “richtige” Tweets, also Tweets mit eigenem Inhalt (keine blanken Retweets)
- Beispiel Account: [https://twitter.com/BayernXr](https://twitter.com/BayernXr)
	- Retweetet Tweets aller bayerischen OGs

## ScheduleTweetBot
- ist noch nicht ganz fertig
- Motivation: eine einfache Möglichkeit einen Haufen Tweets vorzuprogrammieren
- Planen von
	- Text-Tweets
	- Medien-Tweets
	- Text-mit-Medien Tweets
	- alle drei Kombinationen als Thread (geplant)
### How-To
### Text-Tweet
1.  Textdatei, `.txt`, erstellen
	- Namensschema: `YYYYMMDD-hhmm.txt`
	- Beispiel: `20201110-0835.txt`
		- dieser Tweet wird am 10.11.2020 um 8:35 Uhr
2.  Textdatei in den Tweets-Ordner des Bot verschieben
3.  Nach dem Tweeten wird die Datei in den Ordner `old_tweets` verschoben
#### Medien-Tweet
1.  Mediendateie umbenennen
	- Namensschema: `YYYYMMDD-hhmm.[dateiendung]`
		- Beispiel: `20201112-1234.png`
			- dieser Tweet wird am 12.11.2020 um 12:34 Uhr
	- Mehrere Dateien gleichzeitig (keine Videos, Gifs)
		- Namensschema: `YYYYMMDD-hhmm_[zahl].[dateiendung]`
			- Beispiel: `20201110-0835_1.png`, `20201110-0835_2.png`
				 - diese Bilder werden am 10.11.2020 um 8:35 Uhr getweetet
1.  Mediendatei(en) in den Tweets-Ordner des Bot verschieben
2.  Nach dem Tweeten wird/werden die Datei/Dateien in den Ordner `old_tweets` verschoben
#### Text-mit-Medien Tweets
- Kombination aus Text-Tweet und Medien-Tweet
- Textdatei und Mediendatei(en) müssen den selben Namen haben
- Beispiele
	1. Tweet mit einer Mediendatei
		- Textdatei: `20201110-0835.txt`
		- Mediendatei: `20201110-0835.mp4`
	1. Tweet mit mehreren Bilder (bis zu vier)
		- Textdatei: `20201110-0835.txt`
		- Mediendateien: `20201110-0835_1.png`, `20201110-0835_2.jpg`, `20201110-0835_3.jpeg`, `20201110-0835_4.png`

# Tips
## "Kompilieren" mit Nuitka
Wenn die Applikation mit anderen Menschen geteilt werden soll, sollte der API-Key für den Twitterzugang nicht direkt lesbar geteilt werden.
Von dem Benutzer zu verlangen, sich einen eigenen API-Key zu erstellen, ist u.U. zu viel verlangt und imho auch nicht Sinn der Sache.
Um zum einen den API-Key nicht (so leicht) zugänglich zu machen und die Applikation auch für Menschen ohne große Kenntnisse (und Python) zur Verfügung zu stellen, könnte der Code mit *Nuitka* in ein C-Binary kompiliert werden.
### Befehl
`nuitka3 --follow-imports --standalone [BotYouWantToCompile]`
