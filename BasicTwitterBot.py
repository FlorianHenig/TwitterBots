import TwitterApiCredentials
import tweepy
import ast
import time

basic_config_keys = [
    "access_token",
    "access_token_secret",
]
log_file = __file__ + ".log"


class BasicTwitterBot:
    """
    A simple Twitter Bot serving as base class for bots with a real purpose.
    It mainly handles the authentication to the Twitter API.
    """

    class ConfigFile(Exception):
        pass

    class ConfigUnverified(Exception):
        pass

    class ConfigNotLoaded(Exception):
        pass

    class NotAuthenticated(Exception):
        pass

    def __init__(
        self, config_file: str, config_layout_keys: list, logfile: str = log_file
    ):
        self.logfile = logfile
        self.log("__init__")
        self.config_layout_keys = config_layout_keys
        self.config_file = config_file
        self.config_file_verified = False
        self.config = None
        self.auth = None
        self.api = None

    def varify_config_file(self):
        """Verfying the config file."""
        try:
            with open(self.config_file) as f:
                config = ast.literal_eval(f.readline())
                for key in self.config_layout_keys:
                    # Since the acces_token and access_token_secret may be generated later in the authentication process, we ignore them now.
                    if key == "access_token" or key == "access_token_secret":
                        continue
                    if config[key] == "":
                        raise BasicTwitterBot.ConfigFile()
            self.config_file_verified = True
        except BasicTwitterBot.ConfigFile:
            self.log("Config file values empty.")
        except:
            self.log("Error! Config file error. Creating basic config file.")
            self.generate_empty_config_file()
        return self

    def generate_empty_config_file(self):
        """Making a new empty config file based on self.config_layout_keys"""
        self.log("Error! Config file error. Creating basic config file.")
        config = {}
        for key in self.config_layout_keys:
            config[key] = ""
        with open(self.config_file, "w") as f:
            f.write(str(config))

    def load_config(self):
        """Loading the config. Call varify_config_file first!"""
        if not self.config_file_verified:
            raise BasicTwitterBot.ConfigUnverified()
        with open(self.config_file, "r") as f:
            self.config = ast.literal_eval(f.readline())
        return self

    def authenticate(self):
        """Authenticate with Twitter."""
        if self.config is None:
            raise BasicTwitterBot.ConfigNotLoaded()

        # If first start
        if (
            self.config["access_token"] == ""
            or self.config["access_token"] == "access_token_secret"
        ):
            self._allow_app_access_to_twitter()

        self.auth = tweepy.OAuthHandler(
            TwitterApiCredentials.api_key, TwitterApiCredentials.api_secret_key
        )
        self.auth.set_access_token(
            self.config["access_token"], self.config["access_token_secret"]
        )

        self.api = tweepy.API(self.auth)
        return self

    def start(self):
        """Starts the BasicTwitterBot instance."""
        if self.api is None:
            raise BasicTwitterBot.NotAuthenticated()

    def _allow_app_access_to_twitter(self):
        auth = tweepy.OAuthHandler(
            TwitterApiCredentials.api_key, TwitterApiCredentials.api_secret_key
        )
        try:
            redirect_url = auth.get_authorization_url()
            print(__file__ + ": To grant access visit: " + redirect_url)
        except tweepy.TweepError:
            self.log("Error! Failed to get request token.")
            raise
        try:
            pin = input(__file__ + ": Enter PIN from website: ")
            auth.get_access_token(pin)
        except tweepy.TweepError:
            self.log("Error! Verification failed.")

        # Update config and write it to file
        self.config["access_token"] = auth.access_token
        self.config["access_token_secret"] = auth.access_token_secret
        with open(self.config_file, "w") as f:
            f.write(str(self.config))

    def log(self, msg: str):
        """Logging caught errors to a log file."""
        cur_time = time.strftime("%Y%m%d-%H%M%S", time.localtime())
        with open(self.logfile, "a") as f:
            f.write(str(cur_time) + ": " + msg + "\n")
