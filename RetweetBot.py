"""
This is a simple Twitter Bot that has only one task: Retweet every post by every account of the given Twitter lists.
"""
import BasicTwitterBot
import tweepy
import time
import sys

config_layout_expansion = ["lists"]

_config_file = "config.txt"


class RetweetBot(BasicTwitterBot.BasicTwitterBot):
    def __init__(self, config_file: str):
        super().__init__(
            config_file, BasicTwitterBot.basic_config_keys + config_layout_expansion
        )
        self.list_ids = []

    def load_config(self):
        super().load_config()
        # Extracting the different list_id substrings from the comma seperated value of key "lists"
        self.list_ids = self.config[config_layout_expansion[0]].split(sep=",")
        return self

    def start(self):
        """Starts the RetweetBot instance and handles all the retweeting process."""
        super().start()
        since_id = 0
        # Creating a timeline of the list members

        while True:
            # Initial pull of the timeline. After that only the newest tweets will be used by correctly setting the since_id.
            if since_id == 0:
                status_lists = self.get_status_lists(count=20)
            else:
                status_lists = self.get_status_lists()
            for status_list in status_lists:
                for status in status_list:
                    if status.id > since_id:
                        since_id = status.id
                    try:
                        status.retweet()
                    except tweepy.error.TweepError as err:
                        # We expect errors with code 327 aka 'You have already retweeted this Tweet.'
                        # so we can ignore it.
                        # All other tweepy errors will be written to the log file.
                        if err.api_code != 327:
                            self.log(str(err))
                    except:
                        msg = "Unexpected error: " + sys.exc_info()[0]
                        self.log(msg)
                        raise
            time.sleep(60)

    def get_status_lists(self, count=None):
        status_list = []
        for list_id in self.list_ids:
            try:
                if count is None:
                    status_list.append(
                        self.api.list_timeline(list_id=list_id, include_rts=False)
                    )
                else:
                    status_list.append(
                        self.api.list_timeline(
                            list_id=list_id, count=count, include_rts=False
                        )
                    )
            except tweepy.error.TweepError as err:
                msg = str(err) + " : list_id: " + str(list_id)
                self.log(msg)
        return status_list


def main():
    bot = (
        RetweetBot(_config_file)
        .varify_config_file()
        .load_config()
        .authenticate()
        .start()
    )


if __name__ == "__main__":
    # execute only if run as a script
    main()
