#!/bin/bash
# Script for compiling one of the python programs to C code and building a standalon binary.
usage_info="Usage: \"$0 [program.py]\""

if [[ "$1" == "help" ]]; then
 echo "$usage_info"
 exit 0
fi

if [[ -z "$1" ]]; then
 echo "Error: No Python program defined."
 echo "$usage_info"
 exit 1
fi

# Check dependencies
# Check if nuitka3 is installed
pip3 list | grep Nuitka &>/dev/null
if [[ $? -ne 0 ]]; then
 echo "Error: \"Nuitka\" not installed."
 echo -n "Would you like me to install it for you? [y/n]: "
 read ans
 if [[ "$ans" == "y" ]]; then
  pip3 install nuitka
  if [[ $? -ne 0 ]]; then
   exit 1
  fi
 else
  exit 0
 fi
fi

# Check if tweepy is installed
pip3 list | grep Nuitka &>/dev/null
if [[ $? -ne 0 ]]; then
 echo "Error: \"tweepy\" not installed."
 echo -n "Would you like me to install it for you? [y/n]: "
 read ans
 if [[ "$ans" == "y" ]]; then
  pip3 install tweepy
  if [[ $? -ne 0 ]]; then
   exit 1
  fi
 else
  exit 0
 fi
fi

# Setting variable
app_name="$1"

# Check if the file exists
ls ../"$app_name" &>/dev/null
if [[ $? -ne 0 ]]; then
 echo "Error: No such file: \"$app_name\""
fi

mkdir build_files
cp ../* build_files/ &>/dev/null
cd build_files

python3 ../build_api_key.py

nuitka3 --follow-imports --standalone "$app_name"
