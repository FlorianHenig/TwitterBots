#!/bin/python3
api_key_substring = "PUT YOUR API KEY IN HERE"
api_key_secret_substring = "PUT YOUR API SECRET KEY IN HERE"

api_key = input("Twitter API Key: ")
api_key_secret = input("Twitter API Key Secret: ")

new_lines= []
with open("TwitterApiCredentials.py", "r") as f:
    lines=f.readlines()
    for line in lines:
        if line.find(api_key_substring) != -1:
            line = line.replace(api_key_substring, api_key)
        elif line.find(api_key_secret_substring) != -1:
            line = line.replace(api_key_secret_substring, api_key_secret)
        new_lines.append(line)
with open("TwitterApiCredentials.py", "w") as f:
    f.writelines(new_lines)
