import BasicTwitterBot
import tweepy
import time
from pathlib import Path
import shutil
import re
import argparse

config_layout_expansion = ["tweets_folder"]
_config_file = "config.txt"
media_files_suffix = [
    ".jpg",
    ".jpeg",
    ".png",
    ".gif",
    ".mp4",
    ".mpeg",
    ".mpg",
    ".mpe",
    ".webm",
    ".mkv",
    ".mov",
    ".3gp",
    ".avi",
    ".flv",
    ".wav",
    ".flac",
    ".wma",
    ".ac3",
    ".mp3",
]
file_pattern = "^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]"  # Example: 20201101-1945.xyz


class Tweet:
    def __init__(self, api: tweepy.API, media_files: [Path], message_file: Path = None):
        self.api = api
        self._message_file = message_file
        self._media_files = media_files
        self.in_reply_to_status_id = None
        self.auto_populate_reply_metadata = True
        self.attachment_url = None

    def message_file(self) -> Path:
        return self._message_file

    def media_files(self) -> [Path]:
        return self._media_files

    def has_message(self) -> bool:
        return self._message_file is not None

    def has_media(self) -> bool:
        return self._media_files != []

    def tweet(self) -> tweepy.Status:
        try:
            if self.has_message() and self.has_media():
                return self._tweet_message_and_media()
            elif self.has_message():
                return self._tweet_message()
            elif self.has_media():
                return self._tweet_media()
        except tweepy.TweepError as e:
            # Message too long
            if e.api_code == 186:
                if self._message_file:
                    new_path = self._message_file.parent / str(
                        "TextTooLong_" + self._message_file.name
                    )
                    shutil.move(self._message_file, new_path)
                if len(self._media_files) > 0:
                    for f in self._media_files:
                        new_path = f.parent / str("TextTooLong_" + f.name)
                        shutil.move(f, new_path)
            raise

    def reply_to_tweet(self, in_reply_to_status_id) -> tweepy.Status:
        self.in_reply_to_status_id = in_reply_to_status_id

    def _tweet_message_and_media(self) -> tweepy.Status:
        media_ids = self._upload_media_files()
        with open(self._message_file) as f:
            status = f.read()
        return self.api.update_status(
            status,
            media_ids=media_ids,
            in_reply_to_status_id=self.in_reply_to_status_id,
            auto_populate_reply_metadata=self.auto_populate_reply_metadata,
            attachment_url=self.attachment_url,
        )

    def _tweet_message(self) -> tweepy.Status:
        with open(self._message_file) as f:
            status = f.read()
        return self.api.update_status(
            status,
            in_reply_to_status_id=self.in_reply_to_status_id,
            auto_populate_reply_metadata=self.auto_populate_reply_metadata,
            attachment_url=self.attachment_url,
        )

    def _tweet_media(self) -> tweepy.Status:
        media_ids = self._upload_media_files()
        return self.api.update_status(
            media_ids=media_ids,
            in_reply_to_status_id=self.in_reply_to_status_id,
            auto_populate_reply_metadata=self.auto_populate_reply_metadata,
            attachment_url=self.attachment_url,
        )

    def _upload_media_files(self) -> [int]:
        """Uploads the first four media files and returns the media_ids."""
        return [self.api.media_upload(str(f)).media_id for f in self._media_files]

    def reply(self, in_reply_to_status_id) -> tweepy.Status:
        self.in_reply_to_status_id = in_reply_to_status_id
        return self.tweet()


class TwitterThread:
    def __init__(self, api: tweepy.API, tweets: [Tweet]):
        self.api = api
        self.tweets = tweets

    def tweet(self):
        tweet_ids = (
            []
        )  # In case something goes wrong it is better to delete all tweets and retry it later.
        tweet_id = self.tweets.pop(0).tweet().id
        tweet_ids.append(tweet_id)
        try:
            for t in self.tweets:
                tweet_id = t.reply(in_reply_to_status_id=str(tweet_id)).id
                tweet_ids.append(tweet_id)
        except tweepy.error.TweepError:
            # Delete all already posted tweets so tweeting the thread later is possible
            for id in tweet_ids.reverse():
                self.api.destroy_status(str(id))


class ScheduleTweetBot(BasicTwitterBot.BasicTwitterBot):
    def __init__(self, config_file: str):
        super().__init__(
            config_file,
            BasicTwitterBot.basic_config_keys + config_layout_expansion,
            logfile="ScheduleTweetBot.log",
        )
        self.tweets_folder = None
        self.old_tweets_folder = None

    def load_config(self):
        super().load_config()
        self.tweets_folder = Path(self.config[config_layout_expansion[0]])
        self.old_tweets_folder = self.tweets_folder / "old_tweets"
        return self

    def start(self):
        super().start()
        """Starts the RetweetBot instance and handles all the retweeting process."""
        # Make directory for posted tweet files.
        self.old_tweets_folder.mkdir(parents=True, exist_ok=True)
        while True:
            time.sleep(60)
            if self.do_i_have_to_work() is True:
                self.work_next_tweet()

    def work_next_tweet(self):
        files_folders = self.get_all_files_folders()
        if files_folders[0].is_dir():
            try:
                self.tweet_thread(files_folders[0])
                shutil.move(str(files_folders[0].absolute()), self.old_tweets_folder)
            except tweepy.TweepError:
                # Don't remove files when there is an error but rename the thread folder
                new_path = files_folders[0].parent / str(
                    "TextTooLong_" + files_folders[0].name
                )
                shutil.move(files_folders[0], new_path)
                pass
        else:
            try:
                message_file, media_files = self.get_next_message_and_media_files(
                    files_folders
                )
                Tweet(
                    api=self.api, message_file=message_file, media_files=media_files
                ).tweet()
                if message_file is not None:
                    shutil.move(str(message_file.absolute()), self.old_tweets_folder)
                for f in media_files:
                    shutil.move(str(f.absolute()), self.old_tweets_folder)
            except tweepy.TweepError:
                # Don't remove files when there is an error
                pass

    def get_next_message_and_media_files(self, files: [Path]) -> (Path, [Path]):
        base_name = re.search(file_pattern, files[0].stem).group()
        message_file = None
        media_files = []
        # Filter all files for the base_name
        next_files = [x for x in files if x.stem.find(base_name) != -1]
        try:
            message_file = [
                x
                for x in next_files
                if re.search(
                    file_pattern + ".", x.name
                )  # To make sure that there are no files like "20201106-1234_1.txt"
                and x.suffix.lower() == ".txt"
            ].pop(0)
        except IndexError:
            # No message file
            pass
        media_files = [x for x in next_files if x.suffix.lower() in media_files_suffix]
        return (message_file, media_files)

    def get_all_files_folders(self, folder: Path = None) -> [Path]:
        # If no folder is specified use tweets_folder
        if not folder:
            folder = self.tweets_folder
        f = [x for x in folder.iterdir() if re.search(file_pattern, x.name)]
        f.sort()
        return f

    def get_all_files(self, folder: Path, filter: str = None) -> [Path]:
        if filter:
            f = [
                x for x in folder.iterdir() if x.is_file() and re.search(filter, x.name)
            ]
        else:
            f = [x for x in folder.iterdir() if x.is_file()]
        f.sort()
        return f

    def do_i_have_to_work(self) -> bool:
        try:
            scheduled_time = self.get_all_files_folders()[0].stem
            cur_time = time.strftime("%Y%m%d-%H%M", time.localtime())
            return scheduled_time <= cur_time
        except IndexError:
            return False

    def tweet_thread(self, folder: Path):
        # 1. Put message an media files in a list like tweets = [(message_file, media_files)]
        # A post of a thread can contain
        #  - text
        #  - media
        #  - text and media
        # Files for a thread must be named as followed:
        #  - Scheme: "[thread_position][_multiple_image_position].[suffix]"
        #  - Example (Message): "1.txt"
        #  - Example (Media): "1.mp4"
        #    - "1.mp4" will be tweeted together with "1.txt"
        #  - Example (multiple Images): "2_1.jpg", "2_2.png"
        #    - These images will be tweeted together
        #    - Image positions will be the number after "_"
        #    - If there is a "2.txt" file, text will be added to the tweet.
        thread = self.make_thread(self.get_all_files(folder=folder))
        thread.tweet()

    def make_thread(self, files: [Path]) -> TwitterThread:
        # Find all files belonging together
        # Seperate message and media files
        tweet_files = [None]
        for f in files:
            tweet_num = int(re.search("[1-9]+", f.stem).group())
            if f.suffix.lower() == ".txt":
                entry = "message_file"
                try:
                    tweet_files[tweet_num].update({entry: f})
                except IndexError:
                    tweet_files.append({entry: f})
            else:
                entry = "media_files"
                try:
                    media_files = []
                    try:
                        media_files = tweet_files[tweet_num][entry]
                    except KeyError:
                        pass
                    tweet_files[tweet_num].update({entry: media_files + [f]})
                except IndexError:
                    tweet_files.append({entry: [f]})
        tweets = []
        for f in tweet_files[1:]:
            message_file = None
            media_files = []
            try:
                message_file = f["message_file"]
            except KeyError:
                pass
            try:
                media_files = f["media_files"]
            except KeyError:
                pass
            tweets.append(
                Tweet(api=self.api, message_file=message_file, media_files=media_files)
            )
        return TwitterThread(self.api, tweets)


def parse_arguments() -> Path:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "config_file",
        metavar="CONFIGFILE",
        type=str,
        nargs=1,
        help="path of the config file.",
    )
    args = parser.parse_args()
    return args.config_file[0]


def main():
    config_file=parse_arguments()
    exception_count = 0
    while True:
        try:
            bot = (
                ScheduleTweetBot(config_file)
                .varify_config_file()
                .load_config()
                .authenticate()
                .start()
            )
            time.sleep(60)
        except KeyboardInterrupt:
            print
            break
        except BasicTwitterBot.BasicTwitterBot.ConfigUnverified:
            print("Check config file")
            break
        except:
            exception_count += 1
            print("Caught exception #" + str(exception_count))
            if exception_count >= 5:
                print("-> Exiting")
                break
            else:
                pass


if __name__ == "__main__":
    # execute only if run as a script
    main()
